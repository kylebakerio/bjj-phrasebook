1. Internationalization
	- UI terms translated once we have the app and know what terms we need
	- noto font seems to be enough, though datalist seems unsupported
		- need to add for each individual language to do it right
2. DB
	- can't write to sheet, so we should move to something else
	- couchdb + pouchdb... may be overkill, too controlling?
	- simple gcloud mongo + IndexDB persistent request seems lighter/flexible
	- need to finish fleshing out model
	- need to think about gui access... looks like couchdb has a built-in option
3. PWA
	- iOS finally has implemented, but with bugs and caveats, and unclear install there. Still, ours may be simple enough to pull off.
	- otherwise, we can package as needed.
4. TDD
	- framework? tape.js seems promising
	- https://martinfowler.com/articles/is-tdd-dead/
	- https://martinfowler.com/bliki/TestPyramid.html
5. gitlab build/test/deploy (kubernetes)
6. interface:
	- Three Dropdown menus:
		- From Lang (English Default - customizable? remembers last choice?)
		- To Lang ()
		- Topic (All Default)
	- Text Box, options appear below (like PReVo)
7. libs:
	-mithril 
	-[polythene](https://arthurclemens.github.io/polythene-demos/mithril/#/list) (+[milligram](https://milligram.io)?)
	-**probably just CSS, no need for the weight, animations should be simple for now.** Otherwise, web animations api + polyfill for older browsers would be cool.
	-may have to switch from datalist to its polyfill _always_ or some other alternative to support non-OS language fonts in dropdown 
	-webpack probably... though:
	-modernizr for minimizing polyfills, or babel for transpiling?
	-deploy: Travis CI, CircleCI, Codeship, kubernetes, docker?
	-testing + build: look more into tape? + eslint
8.


Scrawled:

light-weight vs. robust display-piece... balance here...
