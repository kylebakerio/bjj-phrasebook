import m from "mithril"
import stream from "mithril/stream"
import { RaisedButton, Dialog, Menu, List, ListTile  } from "polythene-mithril"
import "polythene-css"

import { select, select1 } from '../helpers'
import { LangList, Dictionary, TopicList, loadGSheet } from "./model"

window.Dictionary = Dictionary // debugging

let translatedFromLang = ""
let translatedExpression = ""
let translationPronounciation = ""
let translationTopic = "" // topic from expression
let selectedTopic = "" // topic from user

const resetExpression = () => {
    select1('input').value = ""
    translatedExpression = ""
    translationPronounciation = ""
    translationTopic = ""
}

const dropDown = (options, attributes) => m(
    "select.dropdown",
    attributes,
    options.map(option =>  m(`option[value=${option}]`, option)),
)

// https://github.com/ArthurClemens/polythene/blob/master/docs/components/mithril/menu.md
const dropDownNew = (options, attributes) => m(
    Menu,
    {
      permanent: false,
      content: m(
        List,
        attributes,
        options.map( option => m(ListTile, { title: option }) ),
      )
    },
)

const App = {
	oninit: loadGSheet,
    view: function() {
        return m("main", [
            m("h1", {class: "title"}, "The BJJ Travel Phrasebook"),
            m("label", "Translate From "),
            dropDown(LangList, {id:"from-lang-dropdown", onchange:resetExpression}),
            m("br"),
            m("label", "Topic "),
            dropDown(TopicList, {id:"topic-dropdown", onfocus:resetExpression}),
            m("br"),
            expressionTextBox(),
            m("br"),
            m(RaisedButton, {onclick: translate}, "Translate To"),
            dropDown(LangList, {id:"to-lang-dropdown", onchange:translate}),
            m("br"),
            // m("p", "From " + translatedFromLang),
            m("p", "Topic: " + translationTopic),
            m("p", "Translation: " + translatedExpression),
            translationPronounciation ? m("p", "Pronounciation: " + translationPronounciation) : "",
            m("br"),
            m("br"),
            m("a",{href: "https://goo.gl/D3FKSu"}, "Download PDF"),
            m("br"),
            m("a",{href: "https://www.bjjglobetrotters.com/building-the-bjj-travel-phrasebook/"}, "About the book"),
        ])
    },
}

const expressionTextBox = function() {
    let expressions = []
    if (select1("#from-lang-dropdown") && select1("#from-lang-dropdown").value) {
        translatedFromLang = select1("#from-lang-dropdown").value
        selectedTopic = select1("#topic-dropdown").value

        expressions = Object.keys(Dictionary[translatedFromLang])
        .filter(expression => selectedTopic === "All" || Dictionary[translatedFromLang][expression].topic === selectedTopic)
        .map(expression => m("option", {value: expression}))
    }

    return [
        m("label", "Expression "),
        m("input", {
            onmousedown: resetExpression,
            oninput: translate,
            list:"dictionary_datalist",
            name:"fromExpression",
        }),
        m("datalist", {id:"dictionary_datalist"}, expressions)
    ];
}

const translate = function() {
    const expression = select1('input').value
    const fromLang = select1("#from-lang-dropdown").value
    const toLang = select1("#to-lang-dropdown").value
    if (!Dictionary[fromLang][expression]) return

    // translatedFromLang = Dictionary[expression].sourceLang // get from expression
    translatedFromLang = fromLang // get from user selection
    translatedExpression = Dictionary[fromLang][expression].translateTo[toLang].translations
    translationTopic = Dictionary[fromLang][expression].topic
    translationPronounciation = Dictionary[fromLang][expression].translateTo[toLang].pronounciations ? Dictionary[fromLang][expression].translateTo[toLang].pronounciations : ""
}

m.mount(document.body, App);
