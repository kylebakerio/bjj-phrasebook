import m from "mithril";

const duals = {}
const newRows = {}
let currentRowNumber = 0
let currentTopic = ""

const rowHandlers = {
	blank(row) { return },
	meta_lang(row) { 
		Object.keys(row).forEach(lang => {
			if (lang !== "type" && lang !== "rowNumber") {
				LangList.push(lang)
				Dictionary[lang] = {}
			}
		})
	},
	note(row) { return },
	meta_new(row) {},
	meta_dual(row) {
		// duals are languages that have a translation and a pronounciation
		Object.keys(row).forEach(lang => {
			if (row[lang] === "dual") duals[lang] = true
		})
	},
	meta_new(row) {
		console.error(`update needed (new): ${row.English}, ${row.type}`)
		phrase(row)
	},
	meta_edit(row) {
		console.error(`update needed (edit): ${row.English}, ${row.type}`)
		phrase(row)
	},
	topic(row) {
		currentTopic = row.english
		TopicList.push(currentTopic)
	},
	phrase(row) {
		const currentRow = newRows[currentRowNumber] = Object.assign({}, row)

		Object.keys(row).forEach(col => {				

			if 		(col === "rowNumber") return // discard meta (lib/google)
			else if (col === "type") 	   return // discard meta
			else if (row[col].length === 0) {
				definitionParsers.missingTranslation(col, row.english.trim())
				return
			}

			const lang = col
			
			// if we make it this far, we're storing it.

			// hunt for things:
			// if (currentRow[lang].includes ? currentRow[lang].includes("/") : false) {
			// 	console.warn(`need to handle'/': ${currentRow[lang]}`)
			// }

			if (!duals[lang]) {
			// handling for non-duals
				currentRow[lang] = definitionParsers.basic(row[lang])
			}
			else {
			// handling for duals, and identification for various exceptions in source doc
				const definitionPieces = row[lang].split("\n")
				currentRow[lang] = {};

				if (definitionPieces.length === 3) {
				// dual lang with true dual
					currentRow[lang] = definitionParsers.classicDual(definitionPieces)
				}
				else if (definitionPieces.length === 1) {
				// dual languages with plain english translation, so no pronounciation line needed
					currentRow[lang] = definitionParsers.basic(definitionPieces[0])
				}
				else {
				// dual with "other" row--some kind of irregularity
					console.warn(
						`roughly handled definition: ${lang}(${definitionPieces.length}): ${definitionPieces}`
						)
					currentRow[lang] = definitionParsers.basic(definitionPieces.join("\n"))
				}
			}

			const lookupObject = {
				topic: currentTopic, 	// expression knows own topic
				sourceLang: lang, 		// expression knows own language
				translateTo: currentRow,// expression knows translations of self to other languages
			}

			// expression -> row of all translations by language
			currentRow[lang].translations.forEach(translation => Dictionary[lang][translation] = lookupObject)

			// transliteration is equally searchable for now, if available
			if (currentRow[lang].pronounciations) {
				currentRow[lang].pronounciations.forEach(pronounciation => Dictionary[lang][pronounciation] = lookupObject)
			}
		})

		currentRowNumber++
	},
}

const definitionParsers = {
	missingTranslation(lang, english) {
		console.warn (`no translation to ${lang} of ${english}`)
	},
	basic(def) {
		return { translations: def.split("||").map(term => term.trim()) }
	},
	classicDual(definitionPieces) {
		if (definitionPieces[0].includes("_")) {
			// underscore is used in place of transliteration when translation is simple English loanword
			return { translations: [ definitionPieces[2].trim() ] }
		}
		else {
			return {
				translations: definitionPieces[0].split("||").map(term => term.trim()),
				pronounciations: definitionPieces[2].split("||").map(term => term.trim()),
			}
		}
	},
}

const LangList = []
const TopicList = ["All"]
const Dictionary = {}
const loadGSheet = Tabletop.init.bind(null, {
	key: '1dsdqf0vmAv_389X9anXP7_XN78Xv9EbN3p9yNxU-1kw',
	simpleSheet: true,
	callback: (data, tabletop) => m.redraw(), // console.log("finished loadGSheet", data) 
	postProcess: row => rowHandlers[row.type](row),
})

export { LangList, TopicList, Dictionary, loadGSheet }
