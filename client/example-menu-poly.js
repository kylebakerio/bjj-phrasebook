const { Menu, List, ListTile, RaisedButton, Shadow, IconButton } = polythene;
const stream = window.m.stream;

// Position code

const iconMoreVertSVG = "<svg width=\"24\" height=\"24\" viewBox=\"0 0 24 24\"><path d=\"M12 8c1.1 0 2-.9 2-2s-.9-2-2-2-2 .9-2 2 .9 2 2 2zm0 2c-1.1 0-2 .9-2 2s.9 2 2 2 2-.9 2-2-.9-2-2-2zm0 6c-1.1 0-2 .9-2 2s.9 2 2 2 2-.9 2-2-.9-2-2-2z\"/></svg>";

const PositionMenu = {
	oninit: vnode => {
    const show = stream(false);
    vnode.state = {
      show
    };
  },
  view: vnode => {
    const state = vnode.state;
    const attrs = vnode.attrs;
    const show = state.show();
    return m(".position-container.layout.vertical",
      [
      	vnode.attrs.barPosition === "bottom"
          ? m(".flex")
          : null,
        m(".bar", [
          m(Shadow),
          m(Menu, {
            target: `#${attrs.id}`,
            origin: attrs.origin,
            show,
            didHide: () => state.show(false),
            offset: 8,
            size: 3,
            hideDelay: .240,
            content: m(List, {
              tiles: ["Refresh", "Help & Feedback", "Settings", "Sign Out"].map(title =>
                m(ListTile, {
                  title,
                  ink: true,
                  hoverable: true,
                })
              )
            })
          }),
          m(".bar-row.pe-dark-tone.layout.horizontal", [
            attrs.buttonPosition === "right"
              ? m(".flex") // spacer
              : null,
            m(IconButton, {
              id: attrs.id, // menu target
              icon: { svg: m.trust(iconMoreVertSVG) },
              events: {
                onclick: () => state.show(true)
              }
            })
          ])
        ])
      ]);
  	}
};

// Transitions code
const opener = ({ id, menuOptions }) => ({
	oninit: vnode => {
    const show = stream(false);
    vnode.state = {
      show
    };
  },
  view: vnode => {
    const state = vnode.state;
    const show = state.show();
    return m("div",
      { style: { position: "relative" } },
      [
        m(RaisedButton,
          {
            label: "Open menu",
            id,
            events: {
              onclick: () => state.show(true)
            }
          }
        ),
        m(Menu, Object.assign(
        	{},
          {
          	target: `#${id}`,
            show,
            didHide: () => state.show(false)
          },
          menuOptions
        ))
      ]
    );
	}
});

const simpleList = m(List, [
  m(ListTile, {
    title: "Yes",
    ink: true,
    hoverable: true,
  }),
  m(ListTile, {
    title: "No",
    ink: true,
    hoverable: true,
  })
]);

const TransitionsMenu = opener({
  id: "transitions",
  menuOptions: {
    size: 2, 
    content: simpleList,
    hideDelay: 0.3,
    transitions: {
      show: ({ el }) => ({
        el,
        before: () => (
          el.style.opacity = 0,
          el.style.transform = "translate3d(0, 20px, 0)"
        ),
        transition: () => (
          el.style.opacity = 1,
          el.style.transform = "translate3d(0, 0px,  0)"
        )
      }),
      hide: ({ el }) => ({
        el,
        transition: () => (
          el.style.opacity = 0,
          el.style.transform = "translate3d(0, 20px, 0)"
        )
      })
    }
  }
});

const App = {
  view: () => [
  	m(".row", [
      m(".title", "Position to target button"),
      m(".component", [
      	m(PositionMenu, {
        	id: "tr",
          origin: "top-right",
        	buttonPosition: "right"
        }),
        m(PositionMenu, {
        	id: "bl",
          origin: "bottom-left",
          barPosition: "bottom"
        })
      ])
    ]),
    m(".row", [
      m(".title", "Transitions"),
      m(".component", 
      	m(TransitionsMenu)
      )
    ]),
    m(".row.note", [
    	m("a", {
    		href: "https://jsfiddle.net/ArthurClemens/431659xp/",
        target: "_blank"
	    }, "See also: Mithril Menu fiddles")
    ]),
    m(".row.note", [
    	m("a", {
    		href: "https://arthurclemens.github.io/polythene-demos/mithril/#/menu",
        target: "_blank"
	    }, "See also: Polythene for Mithril - Menu demo")
    ]),
    
  ]
};

m.mount(document.getElementById("root"), App);