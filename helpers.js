
const select = query => {
    const output = document.querySelectorAll(query);
    return output instanceof NodeList ? [...output] : [output]; // nodelist becomes array
};

const select1 = query => select(query)[0];

module.exports = {
	select,
	select1
}
